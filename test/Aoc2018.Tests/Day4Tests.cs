using Xunit;

namespace Aoc2018
{
    public class Day4Tests
    {
        [Theory]
        [InlineData("#1 @ 1,3: 4x4\r\n#2 @ 3,1: 4x4\r\n#3 @ 5,5: 2x2", "4")]
        public void Part1(string data, string expected)
        {
            var day = new Day4();

            var actual = day.Part1(data);

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("#1 @ 1,3: 4x4\r\n#2 @ 3,1: 4x4\r\n#3 @ 5,5: 2x2", "#3")]
        public void Part2(string data, string expected)
        {
            var day = new Day4();

            var actual = day.Part2(data);

            Assert.Equal(expected, actual);
        }
    }
}
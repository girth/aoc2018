using Aoc2018;
using System;
using Xunit;

namespace Aoc2018.Tests
{
    public class Day1Tests
    {
        [Theory]
        [InlineData("+1\r\n+1\r\n+1", "3")]
        [InlineData("+1\r\n+1\r\n-2", "0")]
        [InlineData("-1\r\n-2\r\n-3", "-6")]
        public void Part1(string data, string expected)
        {
            var day = new Day1();

            var actual = day.Part1(data);
            
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("+1\r\n-1", "0")]
        [InlineData("+3\r\n+3\r\n+4\r\n-2\r\n-4", "10")]
        [InlineData("-6\r\n+3\r\n+8\r\n+5\r\n-6", "5")]
        [InlineData("+7\r\n+7\r\n-2\r\n-7\r\n-4", "14")]
        public void Part2(string data, string expected)
        {
            var day = new Day1();

            var actual = day.Part2(data);
            
            Assert.Equal(expected, actual);
        }
    }
}

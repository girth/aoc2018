using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Aoc2018
{
    public class Day2 : IDay
    {
        public string Part1(string rawData)
        {
            var input = Parser.SplitByNewLine(rawData);
            var twoMatchCount = 0;
            var threeMatchCount = 0;
            foreach(var item in input)
            {
                var groups = item.GroupBy(_ => _);
                twoMatchCount += groups.FirstOrDefault(_ => _.Count() == 2) != null ? 1 : 0;
                threeMatchCount += groups.FirstOrDefault(_ => _.Count() == 3) != null ? 1 : 0;
            }

            return (twoMatchCount * threeMatchCount).ToString();
        }

        public string Part2(string rawData)
        {
            var input = Parser.SplitByNewLine(rawData);
            return Parser
                .SplitByNewLine(rawData)
                .Select(_ => 
                    CharInSamePosDiffers(_, input.Where(s => s != _)))
                .First(_ => _.Length > 0);
        }

        private static string CharInSamePosDiffers(string s, IEnumerable<string> items)
        {
            foreach(var item in items)
            {
                for(var i = 0; i < item.Length; i ++)
                {
                    if(s.Remove(i, 1) == item.Remove(i, 1))
                    {
                        return s.Remove(i, 1);
                    }
                }
            }
            return string.Empty;
        }
    }
}
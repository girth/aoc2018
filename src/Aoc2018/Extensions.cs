using System;
using System.Collections.Generic;
using System.Linq;

namespace Aoc2018
{
    public static class IEnumerableExtensions
    {
        public static IEnumerable<T> Cycle<T>(this IEnumerable<T> collection, int index = 0)
        {
            var count = collection.Count();
            index = index % count;
            while(true)
            {
                yield return collection.ElementAt(index);
                index = (index + 1) % count;
            }
        }
    }
}
﻿using LightInject;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Aoc2018
{
    public class Program
    {
        private static Dictionary<string, IDay> _days;

        public static void Main(string[] args)
        {
            Init();

            Console.WriteLine("Select which day to run:");
            Console.WriteLine(string.Join('\n',_days.Select(_ => 
                    string.Concat(_.Key, '\t', _.Value.GetType().Name))));

            var key = Console.ReadLine().Trim();

            var data = Parser.ReadFile($"day{key}.txt");

            Console.WriteLine(string.Concat("Part 1: ", _days[key].Part1(data)));
            Console.WriteLine(string.Concat("Part 2: ", _days[key].Part2(data)));
        }
        
        private static void Init()
        {
            var container = new ServiceContainer();
            container.RegisterAssembly(typeof(IDay).Assembly);
            _days = container
                .GetAllInstances(typeof(IDay))
                .ToLookup(_ => _.GetType().Name.Remove(0, 3))
                .ToDictionary(_ => 
                    _.Key,
                    _ => _.First() as IDay);
        }
    }
}
